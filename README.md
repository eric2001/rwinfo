# rwInfo
![rwInfo](./screenshot.png) 

## Description
rWInfo is a module for [Gallery 3](http://gallery.menalto.com/) which will display information about the current album / photo / video in the sidebar.  It is a modified version of the bundled "Info" module with additional functionality, such as:
 - Removed Title and Description fields (they're displayed elsewhere in the default theme, no reason to show them twice on the same page)
 - Hide the info sidebar for the root album (without title and description there really isn't anything worth displaying here)
 - Display date created for albums only (and continue to display the capture date for everything else)
 - Display tags in the info sidebar (if the tags module is active).
 - Display tags when mousing over the thumbnails (if tags module is active).
 - Display "Movie Info" on movies instead of "Photo Info" like the Gallery Info module does.
 - Use long month instead of short month on the album display.
 - Change Date/Time format to "F j, Y h:i:s a" for photos.
 - Display file size and resolution information for photos and movies.
 - Made "Owner" name field a link to the owners gallery3 profile.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "rwinfo" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

## History
**Version 3.0.0:**
> - Copyright notice updates.
> - Synced rWInfo Code with Gallery 3.0.9 Info Module Code.
> - Released on 29 July 2013.
>
> Download: [Version 3.0.0](/uploads/89821dbae4b754d7821a9543d98255a7/rwinfo300.zip)

**Version 2.2.1:**
> - Bug fixes for user profile links.
> - Released on 20 June 2012.
>
> Download: [Version 2.2.1](/uploads/09cf451dbfb57736b09d7351e237b1ca/rwinfo221.zip)

**Version 2.2.0:**
> - Added link to user's gallery profile (if viewable).
> - Released on 11 June 2012.
>
> Download: [Version 2.2.0](/uploads/100d610627b06feb2b4a478489cf3e46/rwinfo220.zip)

**Version 2.1.0:**
> - Copyright notice updates.
> - Changes to match Gallery 3 api changes.
> - Display file size and resolution for photos and movies.
> - Released on 16 April 2012.
>
> Download: [Version 2.1.0](/uploads/8dfd1cec39e1f14f70c31063a6d0241a/rwinfo210.zip)

**Version 2.0.0:**
> - Updated to match recent changes to the Info module.
> - Released on 28 January 2011.
>
> Download: [Version 2.0.0](/uploads/b07145ea0e86a72ff8061dfbbd56aa6b/rwinfo200.zip)

**Version 1.3.2:**
> - Copyright notice updates.
> - Bugfix for timezones.
> - Released on 04 March 2010.
>
> Download: [Version 1.3.2](/uploads/ec24969c9953a4a1c34950736ae0b795/rwinfo132.zip)

**Version 1.3.1:**
> - Renamed sidebar to "rWInfo" to distinguish from original Info module.
> - Released on 24 February 2010.
>
> Download: [Version 1.3.1](/uploads/6d5836f90c934626de659524555396e8/rwinfo131.zip)

**Version 1.3.0:**
> - Updates for API changes.
> - Fixed tag links.
> - Released on 20 January 2010.
>
> Download: [Version 1.3.0](/uploads/cb1a37615fdad5567a193f6d615546f9/rwinfo130.zip)

**Version 1.2.0:**
> - Updates for changes in current Git code.
> - Released on 14 October 2009.
>
> Download: [Version 1.2.0](/uploads/9ce59c73def89d3123c3376d763b282d/rwinfo120.zip)

**Version 1.1.0:**
> - Updated title to change between Album / Photo / Movie Info.
> - Change Date/Time display for albums vs photos / movies.
> - Released on 16 September 2009.
>
> Download: [Version 1.1.0](/uploads/268224e430b98f0b95a75e6bcfc4b82c/rwinfo110.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 09 September 2009
>
> Download: [Version 1.0.0](/uploads/ee753679848013df3f672cac59cdd2b5/rwinfo100.zip)
